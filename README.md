# Postman

Allows to send messages to any messenger services.

## Install
 - Clone current repository `git clone ...`
 - Move to postman folder `cd postman`
 - Prepare `src/.env` file. Copy from `src/.env.example` and fill as you wish
 - Start cointainers: `docker-compose up -d`
  -- `redis` Redis server for scheduled jobs
  -- `api` Iodine web server for api requests
  -- `sidekiq` Background sidekiq worker
  -- `fake_messenger` It's temprorary server for response to messengers

## API
Request header must contain `API_ACCESS_TOKEN` like in `.env` file.
`POST localhost:3000/api/send`
Post data:
```json
{
  receiver: {
    uuid: "Message Identificator",
    messenger: 'viber', // telegram, viber, whatsapp
  },
  body: 'Text message body',
  callback_url: 'http://microservice/callback',
  attributes: {
    retry_count: 2, //optional
    delay_for: "2018-08-06 00:46:35 +0300"
  }
}
```
Curl example:
```sh
curl -X POST -H "API-ACCESS-TOKEN:example" -d 'receiver[uuid]=e4901416-e5aa-44ec-8970-95b8204a23fe&receiver[messenger]=viber&body=Text message body&callback_url=http://fake:8181/callback&attributes[retry_count]=2' localhost:3000/api/send
```

## Architecture

![architecture](https://raw.githubusercontent.com/vsevolod/urskb/master/public/TZ.png)

##TODO:
 - ~~rubocop with no offenses~~
 - ~~100% rspec coverage (using simplecov)~~
 - Move from raise calls in API to Dry::Monads
 - Add benchmarks
 - Add reek
