module FakeMessenger
  def self.call(_env)
    [200, {}, ['ok']]
  end
end

run FakeMessenger
