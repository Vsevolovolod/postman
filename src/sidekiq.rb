# frozen_string_literal: true

require './config/application'
require './lib/postman'

Sidekiq.configure_server do |config|
  config.redis = { url: ENV['REDIS_URL'] }
end
