# frozen_string_literal: true

require './config/application'
require './lib/postman'

run Postman::Application
