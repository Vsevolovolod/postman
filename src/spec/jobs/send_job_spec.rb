# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Postman::Jobs::SendJob do
  include Dry::Monads::Result::Mixin
  let(:instance) { described_class.new }

  describe '.perform' do
    subject(:perform) { instance.perform(receiver, body, callback_url) }
    let(:receiver) { receiver_attributes }
    let(:body) { 'Sample message' }
    let(:callback_url) { 'http://example.com' }
    let(:operation) do
      double('Postman::Operations::Send', receiver: receiver, call: result)
    end
    let(:result) { Success(receiver) }

    before do
      allow(instance).to receive(:operation).and_return(operation)
    end

    it 'calls send operation' do
      expect(operation).to receive(:call).with([receiver, body])
      expect(Faraday).to receive(:get)

      perform
    end

    context 'failure operation' do
      let(:message) { '504 Error' }
      let(:result) { Failure(message) }

      it 'sends callback about failure operation' do
        expect(Faraday)
          .to receive(:get)
          .with(callback_url, result: 'error', message: message)
        expect { perform }.to raise_error(message)
      end
    end
  end

  describe '.operation' do
    subject(:operation) { instance.operation }
    it { is_expected.to be_kind_of(Postman::Operations::Send) }
  end
end
