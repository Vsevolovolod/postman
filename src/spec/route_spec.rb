# frozen_string_literal: true

require './spec/spec_helper'

RSpec.describe Postman::Application do
  it 'GET /' do
    get '/'

    expect(last_response.status).to eq(404)
    expect(last_response.body).to eq('Not found')
  end
end
