# frozen_string_literal: true

ENV['POSTMAN_ENV'] ||= 'test'

require 'simplecov'
SimpleCov.start

require './config/application'
require './lib/postman'
require 'rack/test'
require 'sidekiq/testing'

Dir['./spec/support/**/*.rb'].each { |f| require f }

RSpec.configure do |config|
  def app
    Postman::Application
  end

  config.include Rack::Test::Methods

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups
end
