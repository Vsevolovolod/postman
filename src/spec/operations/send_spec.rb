# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Postman::Operations::Send do
  subject(:call) { described_class.new.call([receiver, 'Example message']) }
  let(:receiver) { receiver_attributes }
  let(:http_response) { double(success?: true) }

  before do
    allow(Faraday).to receive(:post).and_return(http_response)
  end

  it { is_expected.to be_success }

  context 'undefined messenger' do
    before { receiver[:messenger] = 'whatever' }
    it { is_expected.to be_failure }
  end

  context 'failure message from messenger' do
    let(:http_response) { double(success?: false, status: 502, body: 'Fail') }

    it { is_expected.to be_failure }
  end
end
