# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Postman::Operations::DaemonizeSend do
  describe '#call' do
    subject(:method) { described_class.call(params) }

    context 'with receiver' do
      let(:params) do
        {
          body: 'Some text',
          receiver: receiver_attributes
        }
      end

      it 'stores job' do
        expect { method }.to change { Postman::Jobs::SendJob.jobs.count }.by(1)
      end

      context 'with delay_for attribute' do
        let(:time) { Time.now + 10_000 }
        before { params[:attributes] = { delay_for: time } }

        it 'stores delayed jobs' do
          method

          start_times = Postman::Jobs::SendJob.jobs.map { |job| job['at'] }
          expect(start_times).to include(time.to_i)
        end
      end

      context 'with retry attribute' do
        let(:retry_count) { 3 }
        before { params[:attributes] = { retry_count: retry_count } }

        it 'stores delayed jobs' do
          method

          start_times = Postman::Jobs::SendJob.jobs.map { |job| job['retry'] }
          expect(start_times).to include(retry_count)
        end
      end
    end

    context 'with receivers' do
      let(:params) do
        {
          body: 'Some text',
          receivers: [receiver_attributes, receiver_attributes]
        }
      end

      it 'stores job' do
        expect { method }.to change { Postman::Jobs::SendJob.jobs.count }.by(2)
      end
    end
  end
end
