# frozen_string_literal: true

require 'spec_helper'

RSpec.describe MessageSchema do
  subject(:validator) { described_class.call(attributes) }

  context 'with receiver' do
    let(:attributes) do
      {
        body: 'Text message',
        receiver: receiver_attributes,
        callback_url: 'http://example.com',
        attributes: {
          retry_count: 2,
          delay_for: Time.now
        }
      }
    end

    it { is_expected.to be_success }

    context 'minimum attributes' do
      let(:attributes) do
        {
          body: 'Text message',
          receiver: receiver_attributes
        }
      end

      it { is_expected.to be_success }
    end
  end

  context 'with receivers' do
    let(:attributes) do
      {
        body: 'Text message',
        receivers: [receiver_attributes, receiver_attributes],
        callback_url: 'http://example.com',
        attributes: {
          retry_count: 2,
          delay_for: Time.now
        }
      }
    end

    it { is_expected.to be_success }
  end
end
