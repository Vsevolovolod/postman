# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReceiverSchema do
  subject(:validator) { described_class.call(attributes) }

  let(:attributes) { receiver_attributes }

  it { is_expected.to be_success }

  context 'invalid messenger' do
    before do
      attributes[:messenger] = 'UnsupportedMessenger'
    end

    it { is_expected.to be_failure }
  end
end
