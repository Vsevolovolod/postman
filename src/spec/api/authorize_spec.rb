# frozen_string_literal: true

require './spec/spec_helper'

RSpec.describe Postman::API do
  describe 'GET /api/some_url' do
    context 'without token' do
      it 'returns 401' do
        get '/api/some_url'

        expect(last_response.status).to eq(401)
        expect(last_response.body).to match('not authorize')
      end
    end

    context 'with invalid token' do
      let(:headers) { { 'API_ACCESS_TOKEN' => 'invalid' } }

      it 'returns 401' do
        get '/api/some_url', nil, headers

        expect(last_response.status).to eq(401)
        expect(last_response.body).to match('not authorize')
      end
    end

    context 'with token' do
      include_context :api

      it 'returns 404' do
        get '/api/some_url', nil, headers

        expect(last_response.status).to eq(404)
        expect(last_response.body).to match('Not found')
      end
    end
  end
end
