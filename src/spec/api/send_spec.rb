# frozen_string_literal: true

require './spec/spec_helper'

RSpec.describe Postman::API::Send do
  include_context :api

  describe 'POST /api/send' do
    context 'invalid params' do
      it 'returns 422' do
        post '/api/send', { a: :b }, headers

        expect(last_response.status).to eq(422)
      end
    end

    context 'valid params' do
      let(:params) do
        {
          receiver: receiver_attributes,
          body: 'Text message',
          callback_url: 'http://example.com',
          attributes: {
            retry_count: 2,
            delay_for: Time.now
          }
        }
      end
      let(:request) { post '/api/send', params, headers }

      it 'returns 200' do
        request
        expect(last_response.status).to eq(200)
      end

      context 'with sidekiq job' do
        include_context :sidekiq_inline
        let(:http_response) { double(success?: true) }

        before do
          allow(Faraday).to receive(:post).and_return(http_response)
        end

        it 'returns 200' do
          request
          expect(last_response.status).to eq(200)
        end
      end
    end
  end
end
