# frozen_string_literal: true

require 'securerandom'

module Fixtures
  def receiver_attributes
    {
      uuid: SecureRandom.uuid,
      messenger: 'viber'
    }
  end
end

RSpec.configure do |config|
  config.include Fixtures
end
