# frozen_string_literal: true

RSpec.shared_context :sidekiq_inline do
  around do |example|
    Sidekiq::Testing.inline!(&example)
  end
end
