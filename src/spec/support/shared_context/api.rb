# frozen_string_literal: true

RSpec.shared_context :api do
  let(:headers) { { 'HTTP_API_ACCESS_TOKEN' => ENV['API_ACCESS_TOKEN'] } }
end
