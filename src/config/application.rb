# frozen_string_literal: true

require 'bundler'
require 'json'
require 'dotenv'
require 'dry-monads'
require 'dry/monads/all'

env = ENV.fetch('POSTMAN_ENV', 'development').to_sym
Bundler.require(:default, env)

Dir['./config/*.rb'].each { |file| require file }
