# frozen_string_literal: true

Iodine.threads = ENV.fetch('IODINE_THREADS', 1).to_i
Iodine.workers = ENV.fetch('IODINE_WORKERS', 1).to_i
