# frozen_string_literal: true

# Service for send messages to many messengers
module Postman
  MESSENGERS = %w[viber telegram whatsapp].freeze
end

require './lib/postman/schemas/receiver_schema'
require './lib/postman/schemas/message_schema'
require './lib/postman/jobs/send_job'
require './lib/postman/messengers/base'
require './lib/postman/messengers/viber'
require './lib/postman/messengers/telegram'
require './lib/postman/messengers/whatsapp'
require './lib/postman/operations/daemonize_send'
require './lib/postman/operations/send'
require './lib/postman/application'
require './lib/postman/api'
