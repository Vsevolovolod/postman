# frozen_string_literal: true

module Postman
  module API
    # POST /api/send
    # API Send messages request
    class Send < Base
      def call
        send!(validate!)

        response(200, :ok)
      end

      def validate!
        result = MessageSchema.call(request.params)
        return result.to_hash if result.success?

        raise Postman::API::Errors::UnprocessableEntity, result.messages
      end

      def send!(parameters)
        Postman::Operations::DaemonizeSend.call(parameters)
      end
    end
  end
end
