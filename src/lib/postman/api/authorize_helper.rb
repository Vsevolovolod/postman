# frozen_string_literal: true

module Postman
  module API
    # Compare API_ACCESS_TOKEN header with the same env
    module AuthorizeHelper
      API_ACCESS_TOKEN = 'API_ACCESS_TOKEN'

      def authorize!
        token = request.get_header("HTTP_#{API_ACCESS_TOKEN}")

        return if token && token == ENV[API_ACCESS_TOKEN]

        raise Errors::UnauthorizeError
      end
    end
  end
end
