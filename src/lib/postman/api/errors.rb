# frozen_string_literal: true

module Postman
  module API
    # List of API errors
    module Errors
      UnauthorizeError = Class.new(StandardError)
      UnprocessableEntity = Class.new(StandardError)
      NotFoundError = Class.new(StandardError)
    end
  end
end
