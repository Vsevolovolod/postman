# frozen_string_literal: true

require './lib/postman/api/authorize_helper'

module Postman
  module API
    # Base API class
    class Base
      include AuthorizeHelper

      attr_reader :request

      def initialize(request)
        @request = request
      end

      def call(route)
        catch_exception do
          authorize!

          raise Errors::NotFoundError unless route == '/send' && request.post?

          Postman::API::Send.new(request).call
        end
      end

      private

      def error!(code, error_message)
        response(code, error: error_message)
      end

      def response(code, message)
        [code, {}, [message.to_json]]
      end

      def catch_exception
        yield
      rescue Errors::UnauthorizeError
        error!(401, 'You are not authorize for this request')
      rescue Errors::NotFoundError
        error!(404, 'Not found')
      rescue Errors::UnprocessableEntity => e
        error!(422, e.message)
      end
    end
  end
end
