# frozen_string_literal: true

MessageSchema = Dry::Validation.Params do
  configure do
    def url?(value)
      !URI::DEFAULT_PARSER.make_regexp.match(value).nil?
    end
  end

  optional(:receiver).schema(ReceiverSchema)
  optional(:receivers).each do
    schema(ReceiverSchema)
  end

  required(:body).filled(:str?)
  optional(:callback_url).filled(:url?)

  optional(:attributes).schema do
    # TODO: retry_count by default set to 0
    optional(:retry_count).filled(:int?)
    # TODO: delay_for by default set to Time.now
    optional(:delay_for).filled(:time?)
  end

  rule(some_received: %i[receiver receivers]) do |receiver, receivers|
    receiver.filled? | receivers.filled?
  end
end
