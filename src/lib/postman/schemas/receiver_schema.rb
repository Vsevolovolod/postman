# frozen_string_literal: true

ReceiverSchema = Dry::Validation.Schema do
  configure do
    def messenger_list
      ::Postman::MESSENGERS
    end
  end

  required(:uuid).filled(:str?)
  required(:messenger).filled(included_in?: messenger_list)
end
