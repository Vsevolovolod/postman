# frozen_string_literal: true

module Postman
  module Operations
    # Split message by receivers and send to background
    module DaemonizeSend
      def self.call(params)
        receivers = params[:receivers] || [params[:receiver]]

        job_options = {
          retry: params.dig(:attributes, :retry_count),
          at: params.dig(:attributes, :delay_for).to_i
        }

        receivers.each do |receiver|
          Postman::Jobs::SendJob
            .set(job_options)
            .perform_async(receiver, params[:body], params[:callback_url])
        end
      end
    end
  end
end
