# frozen_string_literal: true

module Postman
  module Operations
    # Send current message for current reseiver
    class Send
      include Dry::Transaction

      step :get_messenger
      step :send_message

      private

      def get_messenger(result)
        receiver, body = result
        messenger = receiver[:messenger]

        unless Postman::MESSENGERS.include?(messenger)
          return Failure("Undefined messenger #{messenger}")
        end

        Success(messenger: messenger, uuid: receiver[:uuid], body: body)
      end

      def send_message(result)
        instance = messenger_klass(result[:messenger]).new
        instance.send(result[:uuid], result[:body])
      end

      def messenger_klass(name)
        Object.const_get("Postman::Messengers::#{name.capitalize}")
      end
    end
  end
end
