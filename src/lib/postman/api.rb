# frozen_string_literal: true

require './lib/postman/api/errors'
require './lib/postman/api/base'
require './lib/postman/api/send'

module Postman
  # API namespace
  module API
  end
end
