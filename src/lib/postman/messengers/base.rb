# frozen_string_literal: true

module Postman
  module Messengers
    # Parent class for send to messengers. Used for fake send only
    class Base
      include Dry::Monads::Result::Mixin

      def send(uuid, body)
        response = Faraday.post(url(uuid), body)

        if response.success?
          Success(receiver(uuid).to_hash)
        else
          Failure("#{response.status} #{response.body}")
        end
      end

      def url(uuid)
        class_name = self.class.to_s.gsub(/^.*::/, '').downcase
        URI("#{ENV['FAKE_URL']}/#{class_name}/#{uuid}")
      end

      def receiver(uuid)
        ReceiverSchema.call(
          uuid: uuid,
          messenger: self.class.name
        )
      end
    end
  end
end
