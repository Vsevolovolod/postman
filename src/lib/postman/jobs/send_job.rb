# frozen_string_literal: true

module Postman
  module Jobs
    # Sends message in background for current receiver
    class SendJob
      include Sidekiq::Worker

      def perform(receiver, body, callback_url)
        result = operation.call([symbolyze!(receiver), body])

        send_callback(callback_url, result) if callback_url

        raise result.failure if result.failure?
      end

      def operation
        Postman::Operations::Send.new
      end

      def send_callback(callback_url, result)
        data =
          if result.success?
            { result: 'success', message: result.value! }
          else
            { result: 'error', message: result.failure }
          end

        Faraday.get(callback_url, data)
      end

      def symbolyze!(old_hash)
        old_hash.each_with_object({}) do |(k, v), new_hash|
          new_hash[k.to_sym] = v
        end
      end
    end
  end
end
