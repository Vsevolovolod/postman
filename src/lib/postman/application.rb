# frozen_string_literal: true

module Postman
  # Module for rack server
  module Application
    module_function

    def call(env)
      request = Rack::Request.new(env)
      api_matcher = request.path.match(api_route)

      # Default routing
      if api_matcher
        Postman::API::Base.new(request).call(api_matcher[1])
      else
        [404, {}, ['Not found']]
      end
    end

    def api_route
      Regexp.new('^/api(/.+)?')
    end
  end
end
